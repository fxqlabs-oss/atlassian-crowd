FROM centos:7

ARG crowdVersion=3.5.0

# Expose Application Data as Volume
VOLUME /var/atlassian/application-data/crowd

# Run Pre-Installation Steps
RUN \
    yum -y install wget diff patch java-1.8.0-openjdk

# Run Installation
RUN \
    echo "Downloading Atlassian Crowd version ${crowdVersion}" && \
    wget -q "https://www.atlassian.com/software/crowd/downloads/binary/atlassian-crowd-${crowdVersion}.tar.gz" && \
    mkdir -p /opt/atlassian && \
    tar xvzf "atlassian-crowd-${crowdVersion}.tar.gz" -C /opt/atlassian && \
    mv "/opt/atlassian/atlassian-crowd-${crowdVersion}" "/opt/atlassian/crowd" && \
    rm -rf "atlassian-crowd-${crowdVersion}.tar.gz"

# Define the Crowd Installation location as the working directory
WORKDIR /opt/atlassian/crowd

RUN \
    chmod +x start_crowd.sh && \
    chmod +x stop_crowd.sh

# Patch the Crowd Home Config
COPY crowdHome.patch crowd-webapp/WEB-INF/classes/crowdHome.patch

RUN \
    patch -b crowd-webapp/WEB-INF/classes/crowd-init.properties -i crowd-webapp/WEB-INF/classes/crowdHome.patch

# Expose the Crowd Port
EXPOSE 8095

# Start the Crowd Service
CMD ./start_crowd.sh -fg